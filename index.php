<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP Intro | Activity 1</title>
</head>
	<h1>Activity 1</h1>
<body>
	<?php
		function getFullAddress($streetAddress, $city, $province, $country) {
			return "$streetAddress, $city, $province, $country";
		}

		echo getFullAddress("804 Atlantic Ave", "Brooklyn", "New York", "United States");
		
		echo("<br />");
		
		echo getFullAddress("Valden House", "Vladivostok", "Primorsky Krai", "Russia");
	?>
</body>
</html>